package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DurationTest {

    private Duration duration;

    @Before
    public void setUp() {
        duration = new Duration();
    }

    @Test
    public void testDur() {
        String expectedDescription = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        Assert.assertEquals("Unexpected description returned.", expectedDescription, duration.dur());
    }
}
