package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MinMaxTest {

    private MinMax minMax;

    @Before
    public void setUp() {
        minMax = new MinMax();
    }

    @Test
    public void testCompare() {
        Assert.assertEquals("Beta should be greater than Alpha.", 6, minMax.compare(3, 6));
        Assert.assertEquals("Alpha should be less than Beta.", -7, minMax.compare(-8, -7));
        Assert.assertEquals("Alpha and Beta should be equal.", 9, minMax.compare(9, 9));
    }

    @Test
    public void testBar() {
        Assert.assertNull("Empty input should return Null.", minMax.bar(null));
        Assert.assertEquals("Non-empty input should return unchanged.", "Hello World!", minMax.bar("Hello World!"));
        Assert.assertEquals("Blank input should return Blank.", "", minMax.bar(""));
    }
}
