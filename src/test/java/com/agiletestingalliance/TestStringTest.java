package com.agiletestingalliance;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestStringTest {

    private final String TEST_STRING = "Sample Text";
    private TestString testString;

    @Before
    public void setUp() {
        testString = new TestString(TEST_STRING);
    }

    @Test
    public void testGstr() {
        assertEquals("Returned string should match initialized string.", TEST_STRING, testString.gstr());
    }

    @Test
    public void testConstructor() {
        assertTrue("The provided string should not be NULL.", testString.string != null);
        assertEquals("The provided string should match the initialised string.", TEST_STRING, testString.string);
    }
}
