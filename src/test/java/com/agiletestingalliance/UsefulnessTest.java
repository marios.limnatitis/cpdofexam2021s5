package com.agiletestingalliance;


import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class UsefulnessTest {

    private Usefulness usefulness;

    @Before
    public void setUp() {
        usefulness = new Usefulness();
    }

    @Test
    public void testDesc() {
        String expectedDescription = "<p>DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br><br>" +
                "<b>CP-DOF </b>helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.</p>";

        assertEquals("Unexpected description returned.", expectedDescription, usefulness.desc());
    }
}
